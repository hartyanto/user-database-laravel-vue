@extends('layouts.master')

@section('content')
    <div id="app" class="relative w-full md:w-4/5 lg:3/5 p-4 mx-auto bg-blue-100">
        <alert-component v-show="alert.show" :message="alert.message" :type="alert.type"></alert-component>
        <modal-component v-show="modal.show" :confirm="deleteUser" :close="closeModalDeleteUser" :title="modal.title" :body="modal.body"></modal-component>
        <h1 class="text-xl mb-4">User Database</h1>
        <div>
            <input v-model="keyword" type="text" class="w-2/3 rounded-lg py-1 px-2" placeholder="Tulis nama yang ingin dicari...">
            <div class="relative w-full">
                <div v-show="showResult" class="absolute top-0 left-0 w-2/3">
                    <div v-if="loadingSearch" class="bg-white p-2">
                        <p>Loading ...</p>
                    </div>
                    <div v-else-if="searchResults.length > 0" v-for="search in searchResults" class="bg-white p-2">
                        <p>@{{ search.name }}</p>
                    </div>
                    <div v-else class="bg-white p-2">
                        <p>Nama tidak ditemukan</p>
                    </div>
                </div>
            </div>
        </div>
        <form class="my-4" action="" method="post" v-on:submit.prevent="submitForm">
            <div class="mb-4">
                <input v-model="newUser" type="text" :class="{'border-red-500' : error.show }" class="w-2/5 p-1 px-2 border rounded-lg bg-gray-50 focus:outline-none focus:border-blue-500">
                <button v-if="loading" type="submit" class="mx-4 py-1 w-28 text-center focus:outline-none rounded-lg bg-blue-500 text-blue-100 hover:bg-blue-600 hover:text-white cursor-wait">Loading ...</button>
                <button v-else type="submit" class="mx-4 py-1 w-28 text-center focus:outline-none rounded-lg bg-blue-500 text-blue-100 hover:bg-blue-600 hover:text-white">@{{ addButton ? 'Add' : 'Update' }}</button>
                <p v-show="error.show" class="text-sm text-red-500">@{{ error.message }}</p>
            </div>
        </form>
        <div>
            <ul class="list-disc ml-10">
                <li class="flex mb-2" v-for="(user, index) in users">
                    <p class="w-10 ml-2 text-center">@{{ index + 1 }}.</p>
                    <p class="w-2/5">@{{ user.name }}</p>
                    <div class="flex">
                        <button class="py-1 w-20 text-center focus:outline-none rounded-lg bg-green-500 text-green-100 hover:bg-green-600 hover:text-white mr-2" v-on:click="editUser(index, user)">Edit</button>
                        <button class="py-1 w-20 text-center focus:outline-none rounded-lg bg-red-500 text-red-100 hover:bg-red-600 hover:text-white" v-on:click="openModalDeleteUser(index, user)">Delete</button>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <template id="alert-template">
        <div class="absolute top-2 right-2 px-4 py-1 rounded-lg text-green-50" :class="type">
            <p>@{{ message }}</p>
        </div>
    </template>

    <template id="modal-template">
        <div class="absolute w-1/2 md:w-1/3 lg:w-1/4 top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 shadow-lg border-2 rounded-lg bg-white">
            <div class="text-center py-1 bg-blue-500 rounded-t-lg">
                <h1 class="text-xl text-white">@{{ title }}</h1>
            </div>
            <div class="flex-col mt-4 px-2 rounded-b-lg">
                <p>@{{ body }}</p>

                <div class="flex justify-end self-end my-4">
                    <button v-on:click="confirm" class="w-24 border hover:border-white bg-blue-500 text-blue-100 rounded-lg shadow-lg hover:bg-blue-600 hover:text-white focus:outline-none mr-4">Confirm</button>
                    <button v-on:click="close" class="w-24 border hover:border-blue-600 bg-blue-100 text-blue-500 rounded-lg shadow-lg hover:bg-white hover:text-blue-600 focus:outline-none">Cancel</button>
                </div>
            </div>
        </div>
    </template>
@endsection


@push('script')
    <script>
        Vue.component('alert-component', {
            template: '#alert-template',
            props: ['message', 'type']
        })

        Vue.component('modal-component', {
            template: '#modal-template',
            props: ['title', 'body', 'confirm', 'close']
        })

        let vm = new Vue ({
            el: '#app',

            data() {
                return {
                    keyword: '',
                    showResult: false,
                    loadingSearch: false,
                    searchResults: [],
                    addButton: true,
                    newUser: '',
                    loading: false, 
                    alert: {
                        show: false,
                        message: '',
                        type: '',
                    },
                    index: {
                            update: null,
                            delete: null,
                        },            
                    id: {
                            update: null,
                            delete: null,
                        },            
                    users: [],
                    error: {
                            show: false,
                            message: ''
                        },
                    modal: {
                        title: '',
                        body: '',
                        confirm: '',
                        show: false,
                    }
                }
            },
            
            watch: {
                keyword: function(newKeyword, oldKeyword) {
                    this.debouncedGetResult()
                }
            },

            created() {
                this.debouncedGetResult = _.debounce(this.search, 500)
            },

            methods: {
                search() {
                    this.loadingSearch = true
                    if( this.keyword.length == 0 ) {
                        this.showResult = false
                    } else {
                        this.showResult = true
                        this.$http.post('/api/user/search', {keyword: this.keyword}).then(response => {
                            this.loadingSearch = false
                            this.searchResults = response.body.data
                        })
                    }
                },

                submitForm: function() {
                    this.error.show = false
                    this.addButton ? this.addUser() : this.updateUser()
                },
                
                addUser: async function() {
                    this.loading = true
                    let input = this.newUser.trim()
                    await this.$http.post('/api/user', {name: input}).then(response => {
                        this.getNewUser()
                        this.newUser = ''
                        this.loading = false
                        this.showAlert('User berhasil ditambahkan!', 'success')
                    }, response => {
                        this.loading = false
                        this.error.show = true
                        this.error.message = response.body.errors.name[0]
                    });
                },
                
                editUser: function(index, user) {
                    this.index.update = index
                    this.id.update = user.id
                    this.addButton = false
                    this.newUser = this.users[index].name
                },

                updateUser: async function() {
                    let input = this.newUser.trim()
                    if(input) {
                        await this.$http.post('/api/user/update/' + this.id.update, {name: input}).then(response => {
                            this.users[this.index.update].name = this.newUser
                            this.newUser = ''
                            this.addButton = true
                            this.showAlert('User berhasil diupdate!', 'primary')
                        });
                    } 
                },

                openModalDeleteUser: function(index, user) {
                    this.modal.show = true
                    this.index.delete = index
                    this.id.delete = user.id
                    this.modal.title = 'Konfirmasi Delete User'
                    this.modal.body = `Anda yakin ingin menghapus user ${this.users[index].name} ?`
                },

                closeModalDeleteUser: function() {
                    this.modal.show = false
                },

                deleteUser: async function() {
                    await this.$http.post('/api/user/delete/' + this.id.delete).then(response => {
                        this.users.splice(this.index.delete, 1)
                        this.closeModalDeleteUser()
                        this.showAlert('User berhasil dihapus!', 'danger')
                    })


                    // let confirm = window.confirm('Anda yakin ?')
                    // if(confirm) {
                    //     await this.$http.post('/api/user/delete/' + user.id).then(response => {
                    //         this.users.splice(index, 1)
                    //         this.showAlert('User berhasil dihapus!', 'danger')
                    //     });
                    // }
                },

                async getUser() {
                    await this.$http.get('/api/user').then(response => {
                        this.users = response.body.data;
                    })
                },

                async getNewUser() {
                    await this.$http.get('/api/user/new-user').then(response => {
                        this.users.unshift(response.body.data);
                    })
                },

                showAlert(message, type = success, duration = 3000) {
                    this.alert.show = true
                    this.alert.message = message
                    switch (type) {
                        case 'success':
                            this.alert.type = 'bg-green-500'
                            break;
                        case 'danger':
                            this.alert.type = 'bg-red-500'
                            break;
                        case 'primary':
                            this.alert.type = 'bg-blue-500'
                            break;
                    
                        default:
                            break;
                    }

                    setTimeout(() => {
                        this.alert.show = false
                    }, duration)
                }
            },

            mounted() {
                this.getUser()
            },
        })
    </script>
@endpush