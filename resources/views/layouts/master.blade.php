<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <title>{{ $title ?? 'Laravel Vue'}}</title>
</head>
<body>
    <div class="relative w-full z-10">
        @include('layouts.partials.navbar')
        @yield('content')
    </div>

    <script src="{{ url('js/script-vue.js') }}"></script>
    <script src="{{ url('js/script-vue-resource.js') }}"></script>
    <script src="{{ url('js/lodash.js') }}"></script>
    {{-- <script src="{{ url('js/script-vue-router.js') }}"></script> --}}
    @stack('script')
</body>
</html>