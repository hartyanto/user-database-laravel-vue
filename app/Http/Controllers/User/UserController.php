<?php

namespace App\Http\Controllers\User;

use App\User;
use Faker\Generator as Faker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('created_at', 'desc')->paginate(10);

        return UserResource::collection($users);
    }

    public function store(Faker $faker)
    {
        request()->validate([
            'name' => ['required', 'min:3'],
        ]);

        User::create([
            'name' => request('name'),
            'email' => $faker->unique()->safeEmail,
            'password' => bcrypt('password'),
        ]);

        return response()->json([
            'message' => 'User berhasil ditambahkan!'
        ]);
    }

    public function update($id)
    {
        $user = User::find($id);
        $user->update([
            'name' => request('name')
        ]);

        return response()->json([
            'message' => 'User berhasil diupdate!'
        ]);
    }

    public function destroy($id)
    {
        User::destroy($id);

        return response()->json([
            'message' => 'User berhasil dihapus!'
        ]);
    }

    public function newUser()
    {
        $user = User::orderBy('created_at', 'desc')->first();

        return new UserResource($user);
    }

    public function search()
    {
        $keyword = request('keyword');
        $user = User::where('name', 'like', '%' . $keyword . '%')->paginate(5);

        return UserResource::collection($user);
    }
}
